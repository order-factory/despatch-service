
currentId = 1;
const nextId = () => {
    return currentId++;
}

module.exports = {
    nextId
};