A simple HTTP Server that enables the dispatching of orders and returns a unique ID.

### Run the application

1. Run the FrontEnd Application:
```
    git clone https://gitlab.com/order-factory/order-app
    cd order-app
    npm install
    npm start
```

2. Run the BackEnd Application:
```
    git clone https://gitlab.com/order-factory/despatch-service
    cd despatch-service
    npm install
    npm start
```

3. Open the Application and start making orders: [http://localhost:3000](http://localhost:3000)
