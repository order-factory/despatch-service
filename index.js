const express = require('express');
const cors = require('cors')
const ordersRouter = require('./api/orders/index');

const app = express();

app.use(cors());
app.use('/api/orders', ordersRouter);

app.listen(8081);
console.log('Listening host:8081');
