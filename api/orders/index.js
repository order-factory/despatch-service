const express = require('express');
const router = express.Router();

const createApi = require('./create');

router.post('/create', createApi);

module.exports = router;
