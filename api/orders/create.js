const database = require('../../database/index');


module.exports = function(request, response) {
    response.json({
        success: true,
        id: database.nextId()
    });
};
